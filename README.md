# mod2vic toolkit

A primitive toolkit for assembling VIC-20 music.

## Overview

1. Make a Protracker module with a nice song, see `HORACE.MOD` for example.
2. Export it to assembly source file `tune.s` with `converter.py`
3. Compile `player.s` with `win2c64`. The song data file `tune.s` is included automatically.
4. `song.prg` now contains your song! 

## Exporter

### Usage

You need to have at least Python 3.3 installed.

	usage: converter.py [-h] [--output OUTPUT] [--tempo TEMPO] [--title TITLE] 
						[--draw_graphics DRAW_GRAPHICS] [--verbose]            
						module                                                 
																			   
	do vic magick                                                              
																			   
	positional arguments:                                                      
	  module                The module where to load data from                 
																			   
	optional arguments:                                                        
	  -h, --help            show this help message and exit                    
	  --output OUTPUT       Where to save the assembly source                  
	  --tempo TEMPO         song tempo (0-255) bigger is slower                
	  --title TITLE         song title displayed on screen                     
	  --draw_graphics DRAW_GRAPHICS                                            
							show text and playback information on screen in the
							resulting binary                                   
	  --verbose                                                                

#### Example
	
	python mod2vic/converter.py HORACE.MOD --output tune.s --title "humongous horace"
	
### Gotchas

* All patterns are exported, even those not appearing in the order list
	
### Channel layout

The imported module must contain 4 channels.

	CHN 1	low pulse (alto)
	CHN 2	medium pulse (tenor)
	CHN 3	high pulse (soprano)
	CHN 4	noise (lion growl!)

Instrument values are ignored by the exporter.

### Supported effect commands

Note: you *cannot* use an effect command and a note on the same row.

	0EC	note cut
	CXX	set global (not channel!) volume
	FXX set speed, valid values 0-1F


## Playroutine
If you run out of RAM (more than 10 patterns of music or so) the song data will be written over VRAM. This is OK but you need to disable all screen operations by setting `--draw_graphics 0`. Good luck.

### Compilation
Place `tune.s` and `song.s` to the same directory and run `win2c64\win2c64.exe -r song.s` 

## Example
See `HORACE.MOD` for a module example. 

Compilation:

	python mod2vic/converter.py HORACE.MOD --output tune.s --title "humongous horace" && 
	del player.prg && win2c64\win2c64.exe -r player.s && move player.rw player.prg 

## License
MIT License, see `COPYING` for details.
